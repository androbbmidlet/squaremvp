package demo.mvp.beautifulapps.squaremvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;
import demo.mvp.beautifulapps.squaremvp.base.BaseActivity;
import demo.mvp.beautifulapps.squaremvp.data.LocationService;
import demo.mvp.beautifulapps.squaremvp.data.PlacesRequester;
import demo.mvp.beautifulapps.squaremvp.data.SquareService;
import demo.mvp.beautifulapps.squaremvp.networking.NetworkServices;
import demo.mvp.beautifulapps.squaremvp.places.PlacesFragment;
import demo.mvp.beautifulapps.squaremvp.places.PlacesPresenter;
import demo.mvp.beautifulapps.squaremvp.util.ActivityUtils;
import demo.mvp.beautifulapps.squaremvp.util.PermissionUtils;
import demo.mvp.beautifulapps.squaremvp.util.Utils;
import demo.mvp.beautifulapps.squaremvp.util.schedulers.SchedulerProvider;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;

import static demo.mvp.beautifulapps.squaremvp.util.PermissionUtils.*;

public class MainActivity extends BaseActivity {

    @Override
    protected int resLayout() {
        return R.layout.activity_main;
    }

    private PlacesPresenter mPlacesPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(PermissionUtils.isPermissionGranted(this) ==false) {

            if (Utils.canGetLocation(this)) {
                mayRequestRuntimePermissions(this);
            } else {
                Utils.showGpsSettingsScreen(this);
            }
        }else{

            if (Utils.canGetLocation(this)) {
                if (PermissionUtils.isPermissionGranted(this) == true){

                       launchView();
            }else{
                    Toast.makeText(this,R.string.gps_rational,Toast.LENGTH_LONG).show();

                }


            } else {
                Utils.showGpsSettingsScreen(this);
            }

        }
    }

    private void launchView() {
        PlacesFragment placesFragment =
                (PlacesFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (placesFragment == null) {
            // Create the fragment
            placesFragment = PlacesFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), placesFragment, R.id.contentFrame);
        }


        SquareService squareService = NetworkServices.provideSquareService("https://api.foursquare.com/v2/");
        PlacesRequester placesRequester = new PlacesRequester(squareService);
        LocationService locationService = new LocationService(getApplicationContext());


        mPlacesPresenter = new PlacesPresenter(placesFragment,SchedulerProvider.getInstance(),placesRequester,locationService);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode ==REQUEST_SETTINGS_LOCATION){

            if(Utils.canGetLocation(this) ==false) {
                Toast.makeText(this,R.string.gps_rational,Toast.LENGTH_LONG).show();
            }else{
                if (PermissionUtils.isPermissionGranted(this) == true){

                    launchView();
                }else{
//                    Toast.makeText(this,R.string.gps_rational,Toast.LENGTH_LONG).show();
                    mayRequestRuntimePermissions(this);


                }
            }

        }

    }

    /**
     * callback for permission requested from OS
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_PERMISSIONS){

            if(PermissionUtils.isPermissionGranted(this)==false){



                Toast.makeText(this,R.string.gps_rational,Toast.LENGTH_LONG).show();

//                finish();
            }else{
                if (Utils.canGetLocation(this)) {

                    if (PermissionUtils.isPermissionGranted(this) == true){

                        launchView();
                    }else{
                        Toast.makeText(this,R.string.gps_rational,Toast.LENGTH_LONG).show();

                    }

                }else{
                    Toast.makeText(this,R.string.location_disabled,Toast.LENGTH_LONG).show();
                }
            }


        }
    }
}
