package demo.mvp.beautifulapps.squaremvp.data;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SquareService {

    @GET("venues/search?client_id=R2GFRJENAEDAASGJBVUZZLIEBDLOOMNP1IY3KSPM3IJILZWY&client_secret=GVT1IOUE0YC0B2UAHCTCP1EJCX0TM3AJNKTQDASM0ZWRDYFG" +
            "&radius=50000&locale=en&limit=50&intent=checkin")
    Single<PlacesResponse> getPlaces(@Query("query") String query, @Query("ll") String lonlat, @Query("v") String currentDate_YYYYMMDD);
}
