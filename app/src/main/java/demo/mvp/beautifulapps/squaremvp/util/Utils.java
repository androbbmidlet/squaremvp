package demo.mvp.beautifulapps.squaremvp.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.Toast;
import demo.mvp.beautifulapps.squaremvp.R;

import static demo.mvp.beautifulapps.squaremvp.util.PermissionUtils.*;


public class Utils {


    private static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            return !TextUtils.isEmpty(locationProviders);
        }
    }



    public static boolean canGetLocation(Context context) {
        return isLocationEnabled(context); // application context
    }

    public static void showAlert(Context context, String title, DialogInterface.OnClickListener onPostiiveOnClickListener){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        AlertDialog alertDialog =alertBuilder.create();
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Ok",onPostiiveOnClickListener);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, R.string.gps_rational,Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        alertDialog.show();




    }
    public static void showGpsSettingsScreen(Activity activity) {

         showAlert(activity, "GpS is not enabled, Launch GPS settings?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                        REQUEST_SETTINGS_LOCATION);
                dialog.dismiss();
            }
        });
    }
}
