package demo.mvp.beautifulapps.squaremvp.places;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import demo.mvp.beautifulapps.squaremvp.R;
import demo.mvp.beautifulapps.squaremvp.model.Place;

import java.util.ArrayList;
import java.util.List;

class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.VenueViewHolder> {

//    private final PlaceClickeListener listener;
    private final List<Place> data = new ArrayList<>();

    PlacesAdapter() {
        setHasStableIds(true);
    }

    @Override
    public VenueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.places_list_item, parent, false);
        return new VenueViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VenueViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).hashCode();
    }

    void setData(List<Place> places) {
        if (places != null) {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new PlacesDiffCallback(data, places));
            data.clear();
            data.addAll(places);
            diffResult.dispatchUpdatesTo(this);
        } else {
            data.clear();
            notifyDataSetChanged();
        }
    }

    static final class VenueViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_repo_name)
        TextView repoNameText;
        @BindView(R.id.tv_repo_description)
        TextView repoDescriptionText;
        @BindView(R.id.tv_fork_count)
        TextView forkCountText;
        @BindView(R.id.tv_distance)
        TextView distance;

        private Place place;

        VenueViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
          /*  itemView.setOnClickListener(v -> {
                if (place != null) {
                    listener.onPlaceClicked(place);
                }
            });*/
        }

        void bind(Place place) {
            this.place = place;
            repoNameText.setText(place.name()!=null?place.name():"");
            repoDescriptionText.setText(place.location().address()!=null? place.location().address():"");

            if(place.location().distance()!=0){
                distance.setText(String.format("%.2f %s",place.location().distance()/1000f," km"));
            }
        }
    }


    interface PlaceClickeListener{
        void onPlaceClicked(Place place);
    }
}
