package demo.mvp.beautifulapps.squaremvp.places;

import com.jakewharton.rxrelay2.BehaviorRelay;
import demo.mvp.beautifulapps.squaremvp.R;
import demo.mvp.beautifulapps.squaremvp.model.Place;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

import java.util.List;

class PlacesViewModel {

    private final BehaviorRelay<List<Place>> placesRelay = BehaviorRelay.create();
    private final BehaviorRelay<Integer> errorRelay = BehaviorRelay.create();
    private final BehaviorRelay<Boolean> loadingRelay = BehaviorRelay.create();

    PlacesViewModel() {

    }

    Observable<Boolean> loading() {
        return loadingRelay;
    }

    Observable<List<Place>> places() {
        return placesRelay;
    }

    Observable<Integer> error() {
        return errorRelay;
    }

    Consumer<Boolean> loadingUpdated() {
        return loadingRelay;
    }

    Consumer<List<Place>> placesUpdated() {
        errorRelay.accept(-1);

        return placesRelay;
    }

    Consumer<Throwable> onError(int error) {

        errorRelay.accept(error);

        return throwable -> {
            Timber.e(throwable, "Error loading Places");
        };
    }
}
