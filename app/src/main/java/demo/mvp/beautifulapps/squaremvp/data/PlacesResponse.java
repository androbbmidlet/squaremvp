package demo.mvp.beautifulapps.squaremvp.data;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import demo.mvp.beautifulapps.squaremvp.model.Place;

import java.util.List;
import java.util.Map;


@AutoValue
public abstract class PlacesResponse {

    @Json(name = "response")
    public abstract Map<String,List<Place>> response();

    public static JsonAdapter<PlacesResponse> jsonAdapter(Moshi moshi) {
        return new AutoValue_PlacesResponse.MoshiJsonAdapter(moshi);
    }


}
