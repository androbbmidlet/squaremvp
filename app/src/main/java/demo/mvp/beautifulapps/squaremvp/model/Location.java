package demo.mvp.beautifulapps.squaremvp.model;

import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

@AutoValue
public abstract class Location {

    public abstract long distance();

    @Nullable
    public abstract String address();


    public static JsonAdapter<Location> jsonAdapter(Moshi moshi) {
        return new AutoValue_Location.MoshiJsonAdapter(moshi);
    }
}
