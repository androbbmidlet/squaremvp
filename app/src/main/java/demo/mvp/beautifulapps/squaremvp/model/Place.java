package demo.mvp.beautifulapps.squaremvp.model;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

@AutoValue
public abstract class Place {

    public abstract String id();

    public abstract String name();

    public abstract Location location();


    public static JsonAdapter<Place> jsonAdapter(Moshi moshi) {
        return new AutoValue_Place.MoshiJsonAdapter(moshi);
    }
}
