package demo.mvp.beautifulapps.squaremvp.util;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class PermissionUtils {

    public static final int REQUEST_PERMISSIONS = 1;
    public static final int REQUEST_SETTINGS_LOCATION = 2;
    private static final String REQUESTED_PERMISSION =ACCESS_FINE_LOCATION;

    public static boolean mayRequestRuntimePermissions(Activity activity) {
       if(isPermissionGranted(activity)){
           return  true;
       }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (activity.shouldShowRequestPermissionRationale(REQUESTED_PERMISSION)) {
//            showMessage(getString(R.string.permission_rationale));
                activity.requestPermissions(new String[]{REQUESTED_PERMISSION}, REQUEST_PERMISSIONS);


            } else {
                activity.requestPermissions(new String[]{REQUESTED_PERMISSION}, REQUEST_PERMISSIONS);
            }
        }
        return false;
    }


    public static boolean isPermissionGranted(Activity activity){

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (activity.checkSelfPermission(REQUESTED_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (activity.checkSelfPermission(REQUESTED_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }




}
