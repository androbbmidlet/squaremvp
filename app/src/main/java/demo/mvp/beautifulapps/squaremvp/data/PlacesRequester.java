package demo.mvp.beautifulapps.squaremvp.data;

import demo.mvp.beautifulapps.squaremvp.model.Place;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

public class PlacesRequester {

    private final SquareService service;

    public PlacesRequester(SquareService service) {

        this.service = service;
    }

    public Single<List<Place>> getPlaces(String query, String lonlat, String currentDate_YYYYMMDD) {
        return

                service.getPlaces(query,lonlat,currentDate_YYYYMMDD)
                        .map(PlacesResponse::response)
                        .map(stringListMap -> {

                           List<Place> placeList =  stringListMap.get("venues");

                            return placeList;
                        })


                .subscribeOn(Schedulers.io());
    }


}
