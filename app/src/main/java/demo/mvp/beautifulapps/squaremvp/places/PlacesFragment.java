package demo.mvp.beautifulapps.squaremvp.places;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import com.jakewharton.rxbinding.widget.RxTextView;
import demo.mvp.beautifulapps.squaremvp.R;
import demo.mvp.beautifulapps.squaremvp.base.BaseFragment;
import demo.mvp.beautifulapps.squaremvp.model.Place;
import io.reactivex.disposables.Disposable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

public class PlacesFragment extends BaseFragment implements PlacesContract.View{


    private PlacesContract.Presenter mPresenter;

    @BindView(R.id.searchTerm)
    EditText searchEditText;

    @BindView(R.id.places_list)
    RecyclerView placesList;
    @BindView(R.id.loading_indicator) View loadingView;
    @BindView(R.id.tv_error)
    TextView errorText;


    public static PlacesFragment newInstance() {

        Bundle args = new Bundle();

        PlacesFragment fragment = new PlacesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);



    }

    @Override
    protected Disposable[] subscriptions() {
        return mPresenter.subscriptions();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void onViewBound(View view) {
        mPresenter.subscribe();
        placesList.setLayoutManager(new LinearLayoutManager(view.getContext()));
        placesList.setAdapter(new PlacesAdapter());

        RxTextView.textChanges(searchEditText)
                .debounce(1500,TimeUnit.MILLISECONDS)
                .subscribe(charSequence -> {

                    String s = charSequence.toString();
                    s= s.trim();
                    if(s.length()<=0){
                        return;
                    }


                    mPresenter.queryTextChanged(s);


                });
    }

    @Override
    public void onPause() {
        super.onPause();


    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.unsubscribe();

    }

    @Override
    protected int resLayout() {
        return R.layout.screen_places;
    }


    @Override
    public void setLoadingIndicator(boolean active) {
        if(active){
            loadingView.setVisibility(View.VISIBLE);
        }else{
            loadingView.setVisibility(View.GONE);
        }

    }

    @Override
    public void showPlaces(List<Place> places) {

        if(placesList.getAdapter() instanceof  PlacesAdapter) {
            errorText.setVisibility(View.GONE);

            PlacesAdapter placesAdapter = (PlacesAdapter) placesList.getAdapter();
            placesAdapter.setData(places);

        }


    }

    @Override
    public void showLoadingPlacesError(String errorMsg) {
        errorText.setText(errorMsg);
        errorText.setVisibility(View.VISIBLE);
    }

    @Override
    public void setPlacesVisibility(boolean placesVisibility) {
        placesList.setVisibility(placesVisibility?View.VISIBLE:View.GONE);
    }

    @Override
    public void setPresenter(PlacesContract.Presenter presenter) {

        mPresenter = checkNotNull(presenter);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.result(requestCode, resultCode);
    }

    @Override
    public void setError(int error) {
        if(error!=-1) {
            errorText.setText(getActivity().getString(error));
            errorText.setVisibility(View.VISIBLE);
        }else{
            errorText.setVisibility(View.GONE);
        }
    }

    @Override
    public void setErrorVisibility(boolean active) {
        errorText.setVisibility(active?View.VISIBLE:View.GONE);
    }
}
