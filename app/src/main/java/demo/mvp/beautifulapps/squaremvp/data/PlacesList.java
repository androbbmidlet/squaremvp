package demo.mvp.beautifulapps.squaremvp.data;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import demo.mvp.beautifulapps.squaremvp.model.Place;

import java.util.List;

@AutoValue
public abstract class PlacesList {

    @Json(name = "venues")
    public abstract List<Place> venues();

    public static JsonAdapter<PlacesList> jsonAdapter(Moshi moshi) {
        return new AutoValue_PlacesList.MoshiJsonAdapter(moshi);
    }
}
