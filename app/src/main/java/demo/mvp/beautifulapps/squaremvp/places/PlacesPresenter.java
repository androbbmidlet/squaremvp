package demo.mvp.beautifulapps.squaremvp.places;


import android.location.Location;
import android.support.annotation.NonNull;
import android.view.View;
import demo.mvp.beautifulapps.squaremvp.R;
import demo.mvp.beautifulapps.squaremvp.data.LocationService;
import demo.mvp.beautifulapps.squaremvp.data.PlacesRequester;
import demo.mvp.beautifulapps.squaremvp.data.SquareService;
import demo.mvp.beautifulapps.squaremvp.networking.NetworkServices;
import demo.mvp.beautifulapps.squaremvp.util.schedulers.BaseSchedulerProvider;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

public class PlacesPresenter implements PlacesContract.Presenter {


    private final PlacesViewModel placesViewModel = new PlacesViewModel();

    private final PlacesRequester placesRequester;
    @NonNull
    private final PlacesContract.View mPlacesView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    @NonNull
    private final LocationService locationService;


    private Location location;
    public PlacesPresenter(
                            @NonNull PlacesContract.View placesView,
                            @NonNull BaseSchedulerProvider schedulerProvider ,
                            @NonNull PlacesRequester placesRequester,
                            @NonNull LocationService locationService) {
        this.placesRequester = placesRequester;
        mPlacesView = checkNotNull(placesView, "placesView cannot be null!");
        mCompositeDisposable = new CompositeDisposable();
        mPlacesView.setPresenter(this);
        mSchedulerProvider = checkNotNull(schedulerProvider, "schedulerProvider cannot be null");
        this.locationService = locationService;

        this.placesViewModel.onError(R.string.waiting_location);


    }







    @Override
    public void result(int requestCode, int resultCode) {

    }

    @Override
    public void queryTextChanged(String query) {
        if(location==null){
            try {
                placesViewModel.loadingUpdated().accept(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            placesViewModel.onError(R.string.location_not_found);

            return;
        }
        loadPlaces(query);

    }
    boolean first;

    private void loadPlaces(String query) {

        String  currentDate_YYYYMMDD  = new SimpleDateFormat("YYYYMMdd").format(new Date());

        String latlan = String.format("%.2f,%.2f",location.getLatitude(),location.getLongitude());

        mCompositeDisposable.add(placesRequester.getPlaces(query,latlan,currentDate_YYYYMMDD)
                .doOnSubscribe(__ -> {placesViewModel.loadingUpdated().accept(true);
                })
                .observeOn(mSchedulerProvider.ui())
                .doOnEvent((d, t) -> {
                    placesViewModel.loadingUpdated().accept(false);
                })
                .subscribe(
                    r->{
                        placesViewModel.placesUpdated().accept(r);}
                , e->{

                    placesViewModel.onError(R.string.api_error_places).accept(e);
                }));


//        mCompositeDisposable.dispose();



    }


    public Disposable[] subscriptions() {
        return new Disposable[]{
                placesViewModel.loading()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(loading -> {
                            mPlacesView.setLoadingIndicator(loading ? true : false);

                            mPlacesView.setPlacesVisibility(loading ? false : true);
                            mPlacesView.setErrorVisibility(loading ? false : true);
                        }),
                placesViewModel.places()
                        .observeOn(mSchedulerProvider.ui())
                        .subscribe(p-> {
                    mPlacesView.showPlaces(p);

                }),
                placesViewModel.error()
                        .observeOn(mSchedulerProvider.ui())
                        .subscribe(errorRes -> {
                    if (errorRes == -1) {

                        mPlacesView.setErrorVisibility( false);
                        mPlacesView.setError(errorRes);


                    } else {

                        mPlacesView.setErrorVisibility( true);
                        mPlacesView.setPlacesVisibility(false);
                        mPlacesView.setError(errorRes);
                    }
                }),
                locationService.error()
                        .observeOn(mSchedulerProvider.ui())
                        .subscribe(errorRes -> {


                            if (errorRes == -1) {

                                mPlacesView.setErrorVisibility( false);
                                mPlacesView.setError(errorRes);


                            } else {

                                mPlacesView.setErrorVisibility( true);
                                mPlacesView.setPlacesVisibility(false);
                                mPlacesView.setError(errorRes);
                            }
                }),
                locationService.loading()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(l -> {
                            mPlacesView.setLoadingIndicator(l ? true : false);

                            mPlacesView.setPlacesVisibility(l ? false : true);
                            mPlacesView.setErrorVisibility(l ? false: true);
                }),
                locationService.loadLocation()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(o -> {System.out.println(o);
                    if(o!=null){

                        this.location =o;
                        placesViewModel.onError(-1);
//                        loadPlaces("hotel");
                    }
                })


        };
    }
    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.dispose();

    }
}
