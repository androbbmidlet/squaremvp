package demo.mvp.beautifulapps.squaremvp.data;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jakewharton.rxrelay2.BehaviorRelay;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;


public class LocationService {
    private final  BehaviorRelay<Location> locationRelay = BehaviorRelay.create();
    private final BehaviorRelay<Integer> errorRelay = BehaviorRelay.create();
    private final BehaviorRelay<Boolean> loadingRelay = BehaviorRelay.create();
    private  Context context;
   public LocationService(Context context){
    this.context =context;
   }


    public Observable<Location> loadLocation() {
       return locationUpdates()
                .doOnSubscribe(__ -> loadingUpdated().accept(true))
                .doOnNext(l->loadingUpdated().accept(false));

    }
    private Observable<Location> locationUpdates(){

                  FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);


                  final LocationRequest locationRequest = new LocationRequest();
                  locationRequest.setInterval(16000);
                  locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


                  final LocationCallback locationCallback = new LocationCallback() {

                      @Override
                      public void onLocationResult(LocationResult locationResult) {
                          super.onLocationResult(locationResult);
                          locationRelay.accept(locationResult.getLastLocation());
                          fusedLocationClient.removeLocationUpdates(this);

                      }


                  };

                  if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                          == PackageManager.PERMISSION_GRANTED) {

                      fusedLocationClient.getLastLocation()
                              .addOnSuccessListener(new OnSuccessListener<Location>() {
                                  @Override
                                  public void onSuccess(Location location) {
                                      // Got last known location. In some rare situations this can be null.
                                      if (location != null) {
                                          // Logic to handle location object
//                                          emitter.onNext(location);
                                          locationRelay.accept(location);

//                                          locationUpdated();
                                      }
                                  }
                              });

                      fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                  } else{
//                      errorRelay.accept(R);
//                      emitter.onError(new Throwable("You haven't given the permissions"));
//                      locationBehaviorRelay.accept(location);
              }

//          emitter.setCancellable(() -> fusedLocationClient.removeLocationUpdates(locationCallback));





//        });

      return locationRelay;

//       locationBehaviorRelay.();


    }
    public Observable<Integer> error() {
        return errorRelay;
    }
    public Observable<Boolean> loading() {
        return loadingRelay;
    }

    Consumer<Boolean> loadingUpdated() {
        return loadingRelay;
    }

    Consumer<Location> locationUpdated() {
        errorRelay.accept(-1);

        return locationRelay;
    }

    Consumer<Throwable> onError() {
        return throwable -> {
            //"You haven't given the permissions"
            Timber.e(throwable, "Error getting  Location");
//            errorRelay.accept("Error getting  Location");
        };
    }

}
