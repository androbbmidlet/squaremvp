package demo.mvp.beautifulapps.squaremvp.networking;

import com.squareup.moshi.Moshi;
import demo.mvp.beautifulapps.squaremvp.data.SquareService;
import demo.mvp.beautifulapps.squaremvp.model.AdapterFactory;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class NetworkServices {

    //https://api.foursquare.com/v2/

    public static Moshi provideMoshi() {
        if(moshiInstance!=null){
            return moshiInstance;
        }
      return   moshiInstance =  new Moshi.Builder()
                .add(AdapterFactory.create())
//                .add(new ResponseAdapter())
                .build();
    }

     private static Retrofit retrofitInstance;
     private static Moshi moshiInstance;

    public static Retrofit getRetrofit( Call.Factory callFactory,  String baseUrl) {

        if(retrofitInstance!=null){
            return retrofitInstance;
        }

        Moshi moshi =provideMoshi();


        retrofitInstance =  new Retrofit.Builder()
                .callFactory(callFactory)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .baseUrl(baseUrl)
                .build();

        return retrofitInstance;
    }

    public static Call.Factory  provideCallFactory(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(chain ->{

                    Response response =  chain.proceed(chain.request());

//                                        byte [] b =response.body().bytes();
//                                        String res = new String(b,"UTF-8");
                    System.out.println(response.toString());

                    return response;

                })

                .build();

        return okHttpClient ;
    }

   public static SquareService provideSquareService(String baseUrl) {
       Retrofit retrofit = getRetrofit(provideCallFactory(),baseUrl);

        return retrofit.create(SquareService.class);
    }
}
